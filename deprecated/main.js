const serverAllowPasskey = ""

const mysql = require("mysql");
const connection = mysql.createConnection({
    host: "localhost",
    user: "nodejs",
    password: "nodejs",
    database: "Posts_ops400xyz" 
});

connection.connect((err) => {
    if(err){
        console.log(`Erro connecting to database... ${err}`)
        return;
    }
    console.log("Connection established!");
});

const express = require("express");
const app =  express();
const port = 5000;

const cors = require("cors");

app.use(cors());

app.use(express.static(__dirname + '/public'));

app.set('views', './views');
app.set('view engine', 'ejs');

app.listen(port, () => {
    console.log(`Hello ${port}`);
});

app.get('/', (_, res) => {
    res.render('index.ejs');
});

app.get('/projetos-sobre', (_, res) => {
    res.render('projetos-sobre.ejs');
});

app.get('/blog-post', (_, res) => {
    let divDeTeste = ``;
    const indicesDaRow = ['titulo', 'dataPublicacao', 'conteudo'];
    connection.query(`select dataPublicacao, titulo, conteudo from Post order by dataPublicacao desc;`, (err, rows) => {
        if(err) {
            divDeTeste = `<div class="alert alert-danger" role="alert" data-bs-theme="dark">
                <p class="fw-bold fs-2 text-center">Erro no banco de dados!!!</p>
            </div>`;
            return;
        }
        else if (!rows.length) {                                                   
            divDeTeste = `<div class="alert alert-danger" role="alert" data-bs-theme="dark">
                <p class="fw-bold fs-2 text-center">Erro no banco de dados!!!</p>
            </div>`;
            return;
        }
        // console.log(rows);
        const rowsJSON = JSON.parse(JSON.stringify(rows));
        divDeTeste = '<div>';
        for(let i = 0; i < rowsJSON.length; i++){
            // console.log(rowsJSON[i].titulo);
            divDeTeste += `
            <div class="post-mae">
                <div class="btn btn-light posts row d-flex align-items-center" data-bs-toggle="collapse" data-bs-target="#post${i}" aria-controls="collapseExample">
                    <p class="fs-5 fw-bold post-button">${rowsJSON[i].titulo}</p>
                    <p class="fs-5 post-button put-end" style="margin-bottom: 0; width: fit-content; margin-left: auto;">${rowsJSON[i].dataPublicacao}</p>
                </div>
                <div class="collapse" id="post${i}" data-bs-theme="dark" style="width: 100% !important;">
                    <div class="card card-body collapse-customizado" style="width: 100% !important;">`;
            let paragrafosSplit = rowsJSON[i].conteudo.split('\n');
            for(let item of paragrafosSplit){
                divDeTeste += `<p class="paragrafo-real">${item}</p>`
            }
            divDeTeste += "</div></div></div>";
                    // </div>
                // </div>
            // </div>
            // `;
        }
        console.log(rows.length);        
        res.render('blog-post.ejs', {mainDiv: divDeTeste});
    });
    // res.render('blog-post.ejs', {mainDiv: '<p style="color: red">Hello</p>'});
});

app.get('/blog-post-create', (_, res) => {
    res.render('blog-post-create.ejs');
});

app.get('/404', (_, res) => {
    res.render('404.ejs');
});

app.get('/erro-cronico-hehe', (_, res) => {
    res.render('erro-cronico-hehe.ejs');
});

app.get('/post/:id', (req, res) => {
    // connection.query(`select titulo, dataPublicacao, conteudo from Post where id=${req.params.id};`, (err, rows) => {
    connection.query(`select conteudo from Post where id=${req.params.id};`, (err, rows) => {
        if(err) {
            res.status(404).send('something wrong');
            return;
        }
        else if (!rows.length) {                                                   
            res.status(404).send('something wrong');
            return;
        }
        
        res.status(200).json(rows);
    });
});

app.get('/post', (req, res) => {
    const passkey = req.query.passkey;

    console.log(req.query);
    console.log(passkey);

    if(passkey == serverAllowPasskey){
        // let query = `insert into Post(titulo, dataPublicacao, conteudo) values('${req.query.titulo}', now(), '${req.query.conteudo}');`;
        // console.log(query);
        // connection.query(query, (err, rows) => {
            // if(err) {
                // console.log(err);
                // res.status(404).send('something wrong');
                // return;
            // }
// 
        // });
        console.log(req.query);
    }
    else{
        console.log("\x1b[31m%s\x1b[0m", "Senha errada");
    }

    console.log(req.query);

    res.end("ok");
});

app.get('/posts-headers', (_, res) => {
    connection.query('select id, titulo, dataPublicacao from Post', (err, rows) => {
        if(err) {
            res.status(404).send('something wrong');
            return;
        }
        else if (!rows.length) {                                                   
            res.status(404).send('something wrong');
            return;
        }
        
        res.status(200).json(rows);
    });
})

process.on("SIGINT", () => {
    connection.end((err) => {
        if(err){
            console.log(`Erro to finish connection... ${err}`);
            return;
        }
        console.log("The connection was finish...");
    });
    process.abort();
});
