class ObjetosNavBar{
    constructor(textoInterno, link){
        this.textoInterno = textoInterno;
        this.link = link;
    }
}

let navbarStringPrincipal = `<nav class="navbar navbar-expand-lg bg-body-tertiary erm-coisas-customizadas" data-bs-theme="dark">
        <div class="container-fluid">
            <a class="navbar-brand fw-bold" style="color: var(--cor-font-p-preto);" href="/">NavBar muito <span class="doce-para-os-olhos">bonita</span></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">`;
let arrayDeObjetosNavBar = [];

arrayDeObjetosNavBar.push(new ObjetosNavBar("Blog", "/blog-post"));
arrayDeObjetosNavBar.push(new ObjetosNavBar("Projetos", "/projetos-sobre"));
arrayDeObjetosNavBar.push(new ObjetosNavBar("Pagina de criação de posts para o \"blog\"", "/blog-post-create"));
arrayDeObjetosNavBar.push(new ObjetosNavBar("404 test", "/404"));
arrayDeObjetosNavBar.push(new ObjetosNavBar("Erro fatal página", "/erro-cronico-hehe"));

let indice = 0;
arrayDeObjetosNavBar.forEach((item) => {
    if(indice == 0)
        navbarStringPrincipal += `<a class="nav-link active" style="color: var(--cor-font-cinza);" href="${item.link}">${item.textoInterno}</a>`
    else
        navbarStringPrincipal += `<a class="nav-link active navbar-item-customizado" href="${item.link}">${item.textoInterno}</a>`
    // navbarStringPrincipal += `<a class="navbar-brand" style="color: var(--cor-font-p-preto);" href="${item.link}">${item.textoInterno}</a>`
    indice++;
});

navbarStringPrincipal += `</div>
                </div>
            </div>
        </nav>
        <div class="hr-entre-nav-main-container"></div>`

const tempGuardarBodyHTML = document.body.innerHTML;
document.body.innerHTML = navbarStringPrincipal + tempGuardarBodyHTML;

window.setInterval(() => {
    const navBarItens = document.getElementsByClassName("nav-link");
    if(window.innerWidth <= 991){
        for(let i = 1; i < navBarItens.length; i++){
            navBarItens[i].classList.remove("navbar-item-customizado");
            navBarItens[i].style.color = "var(--cor-font-cinza)";
        }
    }
    else if(window.innerWidth > 991){
        for(let i = 1; i < navBarItens.length; i++){
            navBarItens[i].style.color = "";
            navBarItens[i].classList.add("navbar-item-customizado");
        }
    }
    else{
        window.location.href = "erro-cronico-hehe.html";
    }
}, 500);