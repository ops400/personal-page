const enderecoAPI = 'localhost:5000';

const textareaElement = document.getElementById("conteudoPost");
const charsSobrandoSpan = document.getElementById("charsSobrando");
const spanTituloStatus = document.getElementById('spanTituloStatus');
const btnBaixar = document.getElementById("btnBaixar");
const tituloTexto = document.getElementById('tituloTexto');
const passkeyText = document.getElementById('passkeyText');

function atualizarCharsSobrando(){
    let textareaElementMaxLenght = textareaElement.maxLength;
    let textareaElementValueLength = textareaElement.value.length;
    let charsSobrando = textareaElementMaxLenght - textareaElementValueLength;
    charsSobrandoSpan.innerText = charsSobrando;

    const spanTextStatus = document.getElementById("spanTextStatus");
    if(charsSobrando >= 10000){
        spanTextStatus.innerText = "O texto não pode estar vazio!";
        spanTextStatus.className = "texto-status-negrito text-vermelho-status";
        btnBaixar.disabled = true;
    }
    else{
        spanTextStatus.innerText = "Conteúdo OK!";
        spanTextStatus.className = "texto-status-negrito text-verde-status";
        btnBaixar.disabled = false;
    }
}

function atualizarSpanTituloStatusECharsSobrando(){
    let tituloTextoMaxLength = tituloTexto.maxLength;
    let tituloTextoValueLength = tituloTexto.value.length;
    let charsSobrando = Number(tituloTextoMaxLength - tituloTextoValueLength);

    const charsSobrandoTitulo = document.getElementById("charsSobrandoTitulo");
    charsSobrandoTitulo.innerText = charsSobrando;
    if(charsSobrando >= 500){
        spanTituloStatus.innerText = 'O título não pode estar vazio';
        spanTituloStatus.className = 'texto-status-negrito text-vermelho-status';
        btnBaixar.disabled = true;
    }
    else{
        spanTituloStatus.innerText = "Título OK!";
        spanTituloStatus.className = "texto-status-negrito text-verde-status";
        btnBaixar.disabled = false;
    }
}

atualizarCharsSobrando();
atualizarSpanTituloStatusECharsSobrando();

textareaElement.addEventListener("keydown", atualizarCharsSobrando);
textareaElement.addEventListener("keyup", atualizarCharsSobrando);
textareaElement.addEventListener("keypress", atualizarCharsSobrando);

tituloTexto.addEventListener("keydown", atualizarSpanTituloStatusECharsSobrando);
tituloTexto.addEventListener("keyup", atualizarSpanTituloStatusECharsSobrando);
tituloTexto.addEventListener("keypress", atualizarSpanTituloStatusECharsSobrando);

let erros = 0;
const xhttp = new XMLHttpRequest();
xhttp.overrideMimeType("text/plain");
xhttp.onreadystatechange = () => {
    if(xhttp.readyState == 4 && xhttp.status == 200){
        // console.log(xhttp.responseText);
        let element = document.createElement('a');
        let numeroDePosts = parseInt(xhttp.responseText);
        numeroDePosts++
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(textareaElement.value));
        element.setAttribute('download', `${numeroDePosts}-bp.txt`);

        element.style.display = 'none';
        document.body.appendChild(element);

        element.click();

        document.body.removeChild(element);
    }
    xhttp.onerror = () => {
        alert(`UM ERRO CRONICO ACONTECEU\n${xhttp.statusText}: ${xhttp.status}`);
        window.location.href = "erro-cronico-hehe.html";
    }
};

// nota de gambiarra: na hora do deploy troca o "localhost" pelo domínio ;D 

btnBaixar.addEventListener("click", async () => {
    alert(passkeyText.value);
    alert(tituloTexto.value);
    alert(textareaElement.value);
});