const enderecoAPI = 'localhost:5000';

let ids = [];

async function pegarJson(url){
    const response = await fetch(url, {
        method: "GET",
    });

    return response.json();
}

function gerarHTMLParaBtnPost(idPost, titulo, dataTime, idDiv){
    const s = `<div id="${idDiv}" class="btn btn-light posts row d-flex align-items-center" onclick="gerenciarPost('${idDiv}', ${idPost});">
                <span class="d-none" id="span${idDiv}">0</span>
                <p class="fs-5 fw-bold post-button">${titulo}</p>
                <p class="fs-5 post-button put-end" style="margin-bottom: 0; width: fit-content; margin-left: auto;">${dataTime}</p>
</div>`
    return s;
}

async function gerarBotoesDosPosts(){
    let postHeaders = [];
    await pegarJson(`http://${enderecoAPI}/posts-headers`).then(postHeadersTemp => { postHeaders = postHeadersTemp;});
    
    // console.log(postHeaders);

    let stringHTMLFinal = '';
    for(let obj of postHeaders){
        // console.log('hello');
        stringHTMLFinal += gerarHTMLParaBtnPost(obj.id, obj.titulo, obj.dataPublicacao, obj.id);
        // console.log(stringHTMLFinal);
    }

    const divPrincial = document.getElementById('divPrincial');

    divPrincial.innerHTML += stringHTMLFinal;
    // console.log(divPrincial);
}

gerarBotoesDosPosts();

function gerenciarPost(idDiv, idPost){    
    // esse span aq faz com que eu saiba se a div já tem o texto ou nao
    // se nao tiver ele é 0, caso contrario ele é 1
    const spanCond = document.getElementById(`span${idDiv}`);

    if(spanCond.innerText == '0'){
        // await pegarJson(`http://${enderecoAPI}/post/${idPost}`).then(post => {
        pegarJson(`http://${enderecoAPI}/post/${idPost}`).then(post => {
            let stringParagrafos = post[0].conteudo;
                
            const sTemp = stringParagrafos.split('\n');
            let HTMLPs = '';
            for(let item of sTemp){
                HTMLPs += `<p class="paragrafo-real">${item}</p>`
            }

            // console.log(HTMLPs);
            const divCont = document.getElementById(`${idDiv}`);
            // console.log(divCont);
            divCont.innerHTML += HTMLPs;
            console.log(spanCond);
        });
        // mudarCond(idDiv);
    }
    spanCond.textContent = '1';
}

function pegarIDs(){
    pegarJson("http://localhost:5000/posts-headers").then(id => { ids = id;});
}