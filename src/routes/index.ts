import { Express, Router, Request, Response } from "express";

const express= require('express');
const router: Router = express.Router();

router.get('/', (_req: Request, res: Response) => {
    res.render('index');
});

export default router;