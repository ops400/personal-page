"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require('express');
const router = express.Router();
router.get('/', (_req, res) => {
    res.render('index');
});
exports.default = router;
