import express, { Application } from 'express';
import path from 'path';
import indexRouter from './routes/index';
const PORT: number = 8080;

const app: Application = express();

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

app.listen(PORT, () => {
    console.log(`Running on port: ${PORT}`);
});